<?php include('../include/head.php'); ?>
<body class="bg-white">
<?php include('../include/navbar.php'); ?>

<header class="container-fluid">
    <div class="row">
        <div class="text-center text-white">
            <h1 class="text_header pt-5 mb-0" data-aos="fade-down" data-aos-duration="2000">Mes Outils d'analyse</h1>  
        </div>
    </div>
</header>
<div class="svg_top" style="background-color: #071a4c;">
    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
        <path fill="#1BAAB0" d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z" class="shape-fill"></path>
    </svg>
</div>

<div class="container-fluid section1 pb-5 pt-5">
    <h2 class="pt-5 pb-3 mb-5 text-center">Savoir concrètement le métier de psychologue</h2> 
    <p class="mb-5 ms-5 me-5">À bien des égards, des tests psychologiques et d’évaluation sont semblables à des tests médicaux. Si un patient présente des symptômes physiques, un fournisseur de soins primaires peut ordonner des rayons X ou des tests sanguins afin de comprendre ce qui cause ces symptômes. <br> <br> Les résultats des tests aideront à informer et à développer un plan de traitement. Les évaluations psychologiques servent le même but. Une psychologue clinicienne utilise les tests et autres outils d’évaluation pour mesurer et observer le comportement d’un patient pour arriver à un diagnostic et un guide de traitement.</p>
    
    <h2 class="mt-5 mb-5 pt-5 pb-3 text-center">Une méthode clinique performante</h2> 
    <p class="mb-5 ms-5 me-5">Une psychologue clinicienne administre des tests et des évaluations pour une grande variété de raisons. Les enfants qui éprouvent des difficultés à l’école, par exemple, peuvent subir des tests d’aptitude pour leurs troubles d’apprentissage. Les tests de compétences telles que la dextérité, le temps de réaction et la mémoire peuvent aider un neuropsychologue à diagnostiquer des confusions telles que des lésions cérébrales ou de la démence. <br><br> Si une personne a des problèmes au travail ou à l’école, ou dans ses relations personnelles, les tests peuvent aider un psychologue à comprendre s’il ou elle pourrait avoir des problèmes avec la gestion de la colère ou des compétences interpersonnelles, ou certains traits de personnalité qui contribuent au problème. D’autres tests évaluent si les patients connaissent des troubles émotionnels comme l’anxiété ou la dépression.</p>

    <h2 class="mt-5 mb-5 pt-5 pb-3 text-center">Les outils d’une psychologue clinicienne</h2> 
    <p class="mb-5 ms-5 me-5 pb-5">Les tests et évaluations sont deux éléments distincts mais connexes d’une évaluation psychologique, utilisés habituellement par une psychologue clinicienne. Les psychologues utilisent les deux types d’outils pour les aider à arriver à un diagnostic et un plan de traitement efficace. <br> Un test implique l’utilisation de tests formels tels que des questionnaires ou des listes de contrôle. <br><br> Ceux-ci sont souvent décrits comme “des tests normatifs”. Cela signifie simplement que les tests ont été normalisés de sorte que les personnes testées sont évalués de la même façon, peu importe où ils vivent ou qui administre le test. Une évaluation psychologique peut inclure de nombreux composants tels que les tests normatifs psychologiques, des tests et des enquêtes informelles, de l’information de l’entrevue, l’école ou les dossiers médicaux, l’évaluation médicale et les données d’observation. <br><br> Une psychologue clinicienne détermine quelles sont les informations à utiliser en fonction des questions spécifiques qui se posent.</p>
</div>


<?php include('../include/footer.php'); ?>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
  AOS.init();
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</body>
</html>