<?php
session_start();
require_once 'config.php'; 

    if (isset($_POST['valider'])) {
        if (!empty($_POST['login']) AND !empty($_POST['password'])) {

            $loginUserEntry = htmlspecialchars($_POST['login']);
            $passwordUserEntry = htmlspecialchars($_POST['password']);

            $statement = $bdd->prepare('SELECT username, password FROM user WHERE username = ?');
            $statement->bindParam(1, $loginUserEntry, PDO::PARAM_STR);
            $statement->execute();
            $loginPassword = $statement->fetch();
            
            if ($loginPassword == false) {
                $_SESSION['error_form'] = "Votre login ou mot de passe est incorrect";
            } else {
                $loginUser = $loginPassword[0];    
                $passwordUser = $loginPassword[1];
    
                //Verification du hash via l'algorithme BCRYPT    
                if (password_verify($passwordUserEntry, $passwordUser)) {
                    $_SESSION['login'] = $loginUser;
                    header('Location: index.php');
                } else {
                    $_SESSION['error_form'] = "Votre login ou mot de passe est incorrect";
                }
            }           
        } else {
            $_SESSION['error_form'] = "Veuillez remplir tous les champs";
        }
    }
?>

<?php include('../include/head.php'); ?>

<body>
    <header class="container-fluid">
        <div class="row">
            <div class="text-center text-white">
                <h1 class="text_header pt-5 mb-0">Espace administrateur</h1>  
            </div>
        </div>
    </header>
    <div class="svg_top">
        <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
            <path fill="#1BAAB0" d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z" class="shape-fill"></path>
        </svg>
    </div>
    <div class="toolbar mt-5 py-3 text-center">
        <img src="assets/icons/logo2.svg" style="width: 100px;" alt="">
        <p class="text-white pt-3" style=" font-size: 25px; font-weight:bold;">Bienvenue sur l'espace de connexion de votre site internet</p>
        <p class="text-white pb-3" style=" font-size: 20px;"> Veuillez entrer votre identifiant et votre mot de passe pour accèder au back office de votre site internet</p>
    </div>
    <div class="container-fluid">
        <h2 class="text-center mt-5 mb-3">Se connecter au back office</h2>

<?php
    if (isset($_SESSION['error_form'])) {
        if ($_SESSION['error_form'] != NULL) {
            echo("<div class=\"alert alert-danger text-center mt-5\" role=\"alert\">" . $_SESSION['error_form'] . "</div>"); 
        } 
        unset($_SESSION['error_form']);
    }

    if (isset($_SESSION['success_form'])) {
        if ($_SESSION['success_form'] != NULL) {
            echo("<div class=\"alert alert-success text-center mt-5\" role=\"alert\">" . $_SESSION['success_form'] . "</div>"); 
        }
        unset($_SESSION['success_form']);
    }
?>

        <form method="POST" action="">
            <div class="row">
                <div class="col">
                    <div class="row d-flex justify-content-center">
                        <div class="col-3">
                            <label for="nom" class="form-label mt-3">Identifiant</label>
                            <input type="text" class="form-control champ_msg" name="login" placeholder="Entrez l'identifiant">
                        </div>          
                        <div class="col-3">
                            <label for="nom" class="form-label mt-3">Mot de passe</label>
                            <input type="password" class="form-control champ_msg" name="password" placeholder="Entrez le mot de passe">
                        </div>          
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="d-flex justify-content-center">
                        <input type="submit" class="btn btn_bar mt-5 mb-3 me-1" name="valider">
                    </div> 
                </div>
            </div>
        </form>
    </div>
</body>
</html>