<?php
session_start(); 
require_once 'config.php';

if(!$_SESSION['login']){
    header('Location: connexion.php');
}
?>

<?php include('../include/head.php'); ?>
<body>
    <header class="container-fluid">
        <div class="row">
            <div class="text-center text-white">
                <h1 class="text_header pt-5 mb-0">Accueil administrateur</h1>  
            </div>
        </div>
    </header>
    <div class="svg_top">
        <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
            <path fill="#1BAAB0" d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z" class="shape-fill"></path>
        </svg>
    </div>
    <div class="text-center pt-5 pb-3">
        <a href="Articles/publier_article.php"><button type="button" class="btn btn_bar mt-3 mb-3 ms-3">Publier un nouvel article</button></a> 
        <a href="Articles/articles.php"><button type="button" class="btn btn_bar mt-3 mb-3 ms-3">Modifier / Supprimer mes articles</button></a>
        <a href="Contact/contact.php"><button type="button" class="btn btn_bar mt-3 mb-3 ms-3">Contact</button></a>
        <a href="logout.php"><button type="button" class="btn btn_bar mt-3 mb-3 ms-3">Déconnexion</button></a>
    </div>

<?php
    $statement = $bdd->prepare('SELECT username FROM user WHERE username = ? ');
    $statement->bindParam(1, $_SESSION['login'], PDO::PARAM_STR);
    $statement->execute();
    $queryResult = $statement->fetch();
    $userNameConnected = $queryResult[0];
?>

    <div class="toolbar mt-3 py-3 text-center">
        <p class="text-white pt-3" style=" font-size: 25px; font-weight:bold;">Bienvenue <?= $userNameConnected; ?> sur l'espace administrateur de votre site internet</p>
        <p class="text-white pb-3" style=" font-size: 20px;">Vous trouverez dans cette espace tout les élèments nécessaires à la gestion de vos articles ainsi que vos formulaires de contact</p>
        <p class="pb-3 pt-5 fst-italic fw-bold" style=" font-size: 25px; color:#1baab0;">Listes des fonctionnalités disponibles :</p>
        <div class="d-flex justify-content-center mt-3">
            <img src="assets/icons/asterisk.svg" style="width: 30px;"alt="">  
            <p class="mb-0 me-5 ms-5 text-white fw-bold">Visualisation des articles</p>
            <img src="assets/icons/asterisk.svg" style="width: 30px;"alt="">  
            <p class="mb-0 me-5 ms-5 text-white fw-bold">Publication de nouveaux articles</p> 
            <img src="assets/icons/asterisk.svg" style="width: 30px;"alt="">  
            <p class="mb-0 me-5 ms-5 text-white fw-bold">Modification du contenu des articles</p>
            <img src="assets/icons/asterisk.svg" style="width: 30px;"alt="">  
            <p class="mb-0 me-5 ms-5 text-white fw-bold">Suppression des articles</p>
        </div>
        <br>
        <div class="d-flex justify-content-center mt-3">
            <img src="assets/icons/asterisk.svg" style="width: 30px;"alt="">  
            <p class="mb-0 me-5 ms-5 text-white fw-bold">Messages reçus</p> 
            <img src="assets/icons/asterisk.svg" style="width: 30px;"alt="">  
            <p class="mb-0 me-5 ms-5 text-white fw-bold">Messages archivés</p>
            <img src="assets/icons/asterisk.svg" style="width: 30px;"alt="">  
            <p class="mb-0 me-5 ms-5 text-white fw-bold">Messages supprimés</p>
        </div>
        <div class="mt-5">
            <a href="../index.php" target="blank"><button type="button" class="btn btn_site mt-3 mb-3 ms-3 px-3 py-2">Mon Site Internet</button></a>
        </div>
    </div>
</body>
</html>