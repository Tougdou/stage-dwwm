<?php
  $recupFormData = $bdd->query('SELECT * FROM contact WHERE (statut = "corbeille") ORDER BY contact.id DESC');
  while ($contact = $recupFormData->fetch()){
?>

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-6">
            <ul class=" list-group mt-5 mb-5">
                <li class="list-group-item fw-bold">
                    <div class="d-flex align-items-center"> 
                        <img style="width: 40px;" class="me-4" src="../assets/icons/calendrier.svg" alt="">
                        <p class="mb-0">Date : <?= $contact['date_time']; ?></p> 
                    </div>
                </li>
                <li class="list-group-item fw-bold">
                    <div class="d-flex align-items-center"> 
                        <img style="width: 40px;" class="me-4" src="../assets/icons/user.svg" alt="">
                        <p class="mb-0">Nom : <?= $contact['nom']; ?></p> 
                    </div>
                </li>
                <li class="list-group-item fw-bold">
                    <div class="d-flex align-items-center"> 
                        <img style="width: 40px;" class="me-4" src="../assets/icons/mail.svg" alt="">
                        <p class="mb-0">Email : <?= $contact['email']; ?></p> 
                    </div>
                </li>
                <li class="list-group-item fw-bold">
                    <div class="d-flex align-items-center"> 
                        <img style="width: 40px;" class="me-4" src="../assets/icons/phone.svg" alt="">
                        <p class="mb-0">Téléphone : <?= $contact['telephone']; ?></p> 
                    </div>
                </li>
                <li class="list-group-item fw-bold li-msg">
                    <div class="d-flex align-items-center justify-content-center"> 
                        <p class="mb-3 mt-3 txt_objet">Objet : <?= $contact['objet']; ?></p>                      
                    </div>
                    <div class="d-flex align-items-center">
                        <img style="width: 40px;" class="me-4 mb-3" src="../assets/icons/message.svg" alt="">
                        <p class="mb-3 word-fix">Message : <?= $contact['message']; ?></p>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid text-center">
        <button type="button" class="btn btn_bar" data-bs-toggle="modal" data-bs-target="#ModalSupp<?= $contact['id'] ?>">Archiver le formulaire</button>
            <div class="modal fade" id="ModalSupp<?= $contact['id'] ?>" tabindex="-1" aria-labelledby="ModalSupp<?= $contact['id'] ?>Label" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title text-dark" id="ModalSupp<?= $contact['id'] ?>Label">Archiver le contact</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body text-dark">
                            <p>Veuillez valider le bouton pour confimer l'archivage du contact choisi.</p>
                        </div>
                        <div class="modal-footer">
                            <a href="archiver_contact.php?id=<?= $contact['id']; ?>">
                                <button type="button" class="btn btn-primary me-1 ms-1">Archiver le contact</button>
                            </a>
                            <button type="button" class="btn btn-dark" data-bs-dismiss="modal">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>

        <button type="button" class="btn btn_bar" data-bs-toggle="modal" data-bs-target="#ModalSupp2<?= $contact['id'] ?>">Supprimer</button>
            <div class="modal fade" id="ModalSupp2<?= $contact['id'] ?>" tabindex="-1" aria-labelledby="ModalSupp2<?= $contact['id'] ?>Label" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title text-dark" id="ModalSupp2<?= $contact['id'] ?>Label">Supprimer le contact</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body text-dark">
                            <p>Veuillez valider le bouton pour confimer la suppression du contact choisi.</p>
                        </div>
                        <div class="modal-footer">
                            <a href="supprimer_contact.php?id=<?= $contact['id']; ?>">
                                <button type="button" class="btn btn-danger me-1 ms-1">Supprimer le contact</button>
                            </a>
                            <button type="button" class="btn btn-dark" data-bs-dismiss="modal">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>
    </div>      
</div>

<?php
    }
?>