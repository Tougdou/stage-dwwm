<?php
session_start(); // Démarrage de la session
require_once '../config.php'; // On inclut la connexion à la base de données

if (!$_SESSION['login']) { // Vérification de connexion
    header('Location: connexion.php'); //Redirection
}

if (isset($_GET['id']) AND !empty($_GET['id'])){
    $getid = $_GET['id'];
    $recupContact = $bdd->prepare('SELECT * FROM contact WHERE id = ?');
    $recupContact->execute(array($getid));

     // Si > à 0 alors l'Contact existe
    if($recupContact->rowCount() > 0) {
        $archiverContact = $bdd->prepare("UPDATE contact SET statut = 'corbeille' WHERE id = ?");
        $archiverContact->execute(array($getid));
        header('Location: contact.php');
    }

}else{
    echo "Aucun identifiant trouver";
}
?>
