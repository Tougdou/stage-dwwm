<?php
  session_start();
  require_once '../config.php';

  if(!$_SESSION['login']){
      header('Location: connexion.php');
  }
?>

<?php include('../include/head.php'); ?>

<body>
    <header class="container-fluid">
        <div class="row">
            <div class="text-center text-white">
                <h1 id="top" class="text_header pt-5 mb-0">Espcace contact</h1>
            </div>
        </div>
    </header>
    <div class="svg_top">
        <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
            <path fill="#1BAAB0" d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z" class="shape-fill"></path>
        </svg>
    </div>
    <div class="text-center pt-3">
        <a href="../index.php"><button type="button" class="btn btn_bar mt-3 mb-3 ms-3">Accueil</button></a> 
        <a href="../Articles/publier_article.php"><button type="button" class="btn btn_bar mt-3 mb-3 ms-3">Publier un nouvel article</button></a>
        <a href="../Articles/articles.php"><button type="button" class="btn btn_bar mt-3 mb-3 ms-3">Modifier / Supprimer mes articles</button></a>
        <a href="../logout.php"><button type="button" class="btn btn_bar mt-3 mb-3 ms-3">Déconnexion</button></a>
    </div>
    <div class="toolbar mt-3 py-3 text-center">
        <p class="text-white pt-3" style=" font-size: 25px; font-weight: bold">Bienvenue sur l'espace principal de votre formulaire</p>
        <p class="text-white pb-3" style=" font-size: 20px;">Vous pourrez consulter l'intégralité du contenu des formulaires remplis sur le site web</p>
    </div>
    
    <div class="container-fluid mt-5">
      <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
          <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">
            <div>
              <img style="width:50px;" src="../assets/icons/mail-new.svg" alt="">
              <p class="pt-3 px-3 title_nav">Messages reçus</p>
<?php
  $statement = $bdd->query('SELECT COUNT(*) FROM contact WHERE statut = "nouveau"');
  $notificationCountNew = $statement->fetch();
?>
                <span class="badge rounded-pill bg-danger"><?= $notificationCountNew[0] ?></span>
            </div>
          </button>
        </li>
        <li class="nav-item" role="presentation">
        
        <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">
            <div>
              <img style="width:50px;" src="../assets/icons/mail-archive.svg" alt="">
              <p class="pt-3 px-3 title_nav">Messages archivés</p>         
<?php
  $statement = $bdd->query('SELECT COUNT(*) FROM contact WHERE statut = "archive"');
  $notificationCountArchive = $statement->fetch();
?>
                <span class="badge rounded-pill bg-primary"><?= $notificationCountArchive[0] ?></span>
            </div>
          </button>
        </li>
        <li class="nav-item" role="presentation">
          <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">
            <div>
              <img style="width:50px;" src="../assets/icons/corbeille.svg" alt="">
              <p class="pt-3 px-3 title_nav">Corbeille</p>
<?php
  $statement = $bdd->query('SELECT COUNT(*) FROM contact WHERE statut = "corbeille"');
  $notificationCountCorbeille = $statement->fetch();
?>
              <span class="badge rounded-pill bg-light text-dark"><?= $notificationCountCorbeille[0] ?></span>
            </div>  
          </button>
        </li>
      </ul>
      <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

          <?php include('msg_reçus.php'); ?>
        </div>
        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
          <?php include('msg_archives.php'); ?>
        </div>
        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
          <?php include('msg_corbeille.php'); ?>
        </div>
      </div>
    </div>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</body>
</html>
