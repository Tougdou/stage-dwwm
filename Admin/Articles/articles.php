<?php
session_start(); // Démarrage de la session
require_once '../config.php'; // On inclut la connexion à la base de données

if (!$_SESSION['login']) { // Vérification de connexion
    header('Location: connexion.php'); //Redirection
}
?>

<?php include('../include/head.php'); ?>

<body>
    <header class="container-fluid">
        <div class="row">
            <div class="text-center text-white">
                <h1 id="top" class="text_header pt-5 mb-0">Mes articles</h1>
            </div>
        </div>
    </header>
    <div class="svg_top">
        <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
            <path fill="#1BAAB0" d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z" class="shape-fill"></path>
        </svg>
    </div>
    <div class="text-center pt-3">
        <a href="../index.php"><button type="button" class="btn btn_bar mt-3 mb-3 ms-3">Accueil</button></a> 
        <a href="publier_article.php"><button type="button" class="btn btn_bar mt-3 mb-3 ms-3">Publier un nouvel article</button></a>
        <a href="../Contact/contact.php"><button type="button" class="btn btn_bar mt-3 mb-3 ms-3">Contact</button></a>
        <a href="../logout.php"><button type="button" class="btn btn_bar mt-3 mb-3 ms-3">Déconnexion</button></a>
    </div>
    <div class="toolbar mt-3 py-3 text-center">
        <p class="text-white pt-3" style=" font-size: 25px; font-weight: bold">Bienvenue sur l'espace principal de vos articles</p>
        <p class="text-white pb-3" style=" font-size: 20px;">Vous pourrez consulter l'intégralité de vos articles ainsi que modifier et supprimer chacun d'entre eux</p>
    </div>

    <?php
    $recupArticles = $bdd->query('SELECT * FROM articles ORDER BY articles.id DESC');
    while ($article = $recupArticles->fetch()){
    ?>

        <div class="articles mx-5 mt-5 mb-5">
            <h1 class="text-center py-5 px-5"><?= $article['titre']; ?></h1>
            <p class="mx-5"><?= nl2br($article['contenu']) ; ?></p>
            <div class="text-center py-3">
                <button type="button" class="btn btn_bar mx-1 my-5" data-bs-toggle="modal" data-bs-target="#ModalModif<?= $article['id'] ?>">Modifier l'article</button>
                <div class="modal fade" id="ModalModif<?= $article['id'] ?>" tabindex="-1" aria-labelledby="ModalModif<?= $article['id'] ?>Label" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title text-dark" id="ModalModif<?= $article['id'] ?>Label">Modifier mon article</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body text-dark">
                                <p>Veuillez valider le bouton pour confimer la modification de l'article choisi.</p>
                            </div>
                            <div class="modal-footer">
                                <a href="modifier_article.php?id=<?= $article['id']; ?>">
                                    <button type="button" class="btn btn-primary me-1 ms-1">Modifier l'article</button>
                                </a>
                                <button type="button" class="btn btn-dark" data-bs-dismiss="modal">Fermer</button>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn_bar mx-1 my-5" data-bs-toggle="modal" data-bs-target="#ModalSupp<?= $article['id'] ?>">Supprimer l'article</button>
                <div class="modal fade" id="ModalSupp<?= $article['id'] ?>" tabindex="-1" aria-labelledby="ModalSupp<?= $article['id'] ?>Label" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title text-dark" id="ModalSupp<?= $article['id'] ?>Label">Supprimer mon article</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body text-dark">
                                <p>Veuillez valider le bouton pour confimer la suppression de l'article choisi.</p>
                            </div>
                            <div class="modal-footer">
                                <a href="supprimer_article.php?id=<?= $article['id']; ?>">
                                    <button type="button" class="btn btn-danger me-1 ms-1">Supprimer l'article</button>
                                </a>
                                <button type="button" class="btn btn-dark" data-bs-dismiss="modal">Fermer</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php
    }
    ?>
    <div class="text-center mb-5">
        <a href="#top">
            <img src="assets/images/fleche.png" alt="">
        </a>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</body>

</html>