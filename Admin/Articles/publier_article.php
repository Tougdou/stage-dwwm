<?php
session_start(); // Démarrage de la session
require_once '../config.php'; // On inclut la connexion à la base de données

if (!$_SESSION['login']) { // Vérification de connexion
    header('Location: connexion.php'); //Redirection
}

if (isset($_POST['envoi'])){
    if(!empty($_POST['titre']) AND !empty($_POST['contenu'])){
        $titre = htmlspecialchars($_POST['titre']);
        $contenu = htmlspecialchars($_POST['contenu']);

        $insererArticle = $bdd ->prepare('INSERT INTO articles(titre, contenu)VALUES(?,?)');
        $insererArticle->execute(array($titre, $contenu));

        $_SESSION['success_form'] = "L'article a été bien envoyé";
    }else{
        $_SESSION['error_form'] = "Veuillez remplir tous les champs";
    }
}

?>

<?php include('../include/head.php'); ?>
<body>
    <header class="container-fluid">
        <div class="row">
            <div class="text-center text-white">
                <h1 class="text_header pt-5 mb-0">Publier mon article</h1>  
            </div>
        </div>
    </header>
    <div class="svg_top">
        <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
            <path fill="#1BAAB0" d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z" class="shape-fill"></path>
        </svg>
    </div>
    <div class="text-center pt-3">
        <a href="../index.php"><button type="button" class="btn btn_bar mt-3 mb-3 ms-3">Accueil</button></a> 
        <a href="articles.php"><button type="button" class="btn btn_bar mt-3 mb-3 ms-3">Modifier / Supprimer mes articles</button></a>
        <a href="../Contact/contact.php"><button type="button" class="btn btn_bar mt-3 mb-3 ms-3">Contact</button></a>
        <a href="../logout.php"><button type="button" class="btn btn_bar mt-3 mb-3 ms-3">Déconnexion</button></a>
    </div>
    <div class="toolbar mt-3 py-3 text-center">
        <p class="text-white pt-3" style=" font-size: 25px; font-weight:bold;">Bienvenue sur l'espace de publication de vos articles</p>
        <p class="text-white pb-3" style=" font-size: 20px;"> Veuillez remplir les deux champs présent du formulaire afin de mettre votre futur article en ligne</p>
    </div>
    <?php
    if (isset($_SESSION['error_form'])) {
                            if ($_SESSION['error_form'] != NULL) {
                            echo("<div class=\"alert alert-danger text-center mt-5\" role=\"alert\">" . $_SESSION['error_form'] . "</div>"); 
                            }
                            unset($_SESSION['error_form']);
                        }
    if (isset($_SESSION['success_form'])) {
        if ($_SESSION['success_form'] != NULL) {
        echo("<div class=\"alert alert-success text-center mt-5\" role=\"alert\">" . $_SESSION['success_form'] . "</div>"); 
        }
        unset($_SESSION['success_form']);
    }
    ?>
    <div class="container-fluid">
        <form method="POST" action="">
        <div class="row d-flex justify-content-center">
            <div class="col-6">
                <label for="titre" class="form-label mt-5">Titre</label>
                <div>
                    <input type="text" class="col-12 champ_msg form-control" id="titre" name="titre" placeholder="Titre de l'article"></input>    
                </div> 
            </div>          
        </div>
        <br>
        <div class="row d-flex justify-content-center mb-3">
            <div class="col-6">
                <label for="contenu" class="form-label">Contenu</label>
                <div>
                    <textarea class="champ_msg form-control" rows="6" id="contenu" name="contenu" placeholder="Contenu de votre article"></textarea>    
                </div> 
            </div>          
        </div>
        <br>
        <div class="row d-flex justify-content-center">
            <div class="col-10 text-center">
                <input type="submit" name="envoi" class="btn btn_form mb-3"></input>
                <input type="reset" class="btn btn_form mb-3" value="Réinitialiser"></input>    
            </div>           
        </div>
        </form>
    </div>

</body>
</html>