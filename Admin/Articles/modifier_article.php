<?php
session_start(); // Démarrage de la session
require_once '../config.php'; // On inclut la connexion à la base de données

if (!$_SESSION['login']) { // Vérification de connexion
    header('Location: connexion.php'); //Redirection
}


if(isset($_GET['id']) AND !empty($_GET['id'])){
    $getid = $_GET['id'];

    $recupArticle = $bdd->prepare('SELECT * FROM articles WHERE id = ?');
    $recupArticle->execute(array($getid));
    if($recupArticle->rowCount() > 0){
        $articleInfos = $recupArticle->fetch();
        $titre = $articleInfos['titre'];
        $contenu = $articleInfos['contenu'];
        if(isset($_POST['envoi'])){
            $titre_saisi = htmlspecialchars($_POST['titre']);
            $contenu_saisi = htmlspecialchars($_POST['contenu']);

            $updateArticle = $bdd->prepare('UPDATE articles SET titre = ?, contenu = ? WHERE id = ?');
            $updateArticle->execute(array($titre_saisi, $contenu_saisi, $getid));

            header('Location: articles.php'); 
        }

    }else{
        echo "Aucun article trouver";
    }

}else{
    echo "Aucun identifiant trouvé";
}
?>


<?php include('../include/head.php'); ?>

<body>
    <header class="container-fluid">
        <div class="row">
            <div class="text-center text-white">
                <h1 class="text_header pt-5 mb-0">Modifier mon article</h1>  
            </div>
        </div>
    </header>
        <div class="svg_top">
            <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
                <path fill="#1BAAB0" d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z" class="shape-fill"></path>
            </svg>
        </div>
        <div class="text-center pt-3">
            <a href="index.php"><button type="button" class="btn btn_bar mt-3 mb-3 ms-3">Accueil</button></a> 
            <a href="articles.php"><button type="button" class="btn btn_bar mt-3 mb-3 ms-3">Modifier / Supprimer mes articles</button></a>
            <a href="../Contact/contact.php"><button type="button" class="btn btn_bar mt-3 mb-3 ms-3">Contact</button></a>
            <a href="logout.php"><button type="button" class="btn btn_bar mt-3 mb-3 ms-3">Déconnexion</button></a>
        </div>
        <div class="toolbar mt-3 py-3 text-center">
            <p class="text-white pt-3" style=" font-size: 25px; font-weight:bold;">Bienvenue sur l'espace de modification de vos articles</p>
            <p class="text-white pb-3" style=" font-size: 20px;">Vous pouvez mettre à jour votre article en modifiant le texte présent dans les deux champs du formulaire</p>
        </div>
        <div class="container-fluid">
            <form method="POST" action="">
                <div class="row d-flex justify-content-center">
                    <div class="col-6">
                        <label for="titre" class="form-label mt-5">Titre de l'article</label>
                        <div>
                            <input type="text" name="titre" value="<?= $titre; ?>" class="col-12 champ_msg form-control"></input>    
                        </div> 
                    </div>          
                </div>
                <br>
                <div class="row d-flex justify-content-center mb-3">
                    <div class="col-6">
                        <label for="contenu" class="form-label">Contenu de l'article</label>
                        <div>
                            <textarea class="champ_msg form-control" rows="20" id="contenu" name="contenu" ><?= $contenu; ?>   
                            </textarea>    
                        </div> 
                    </div>          
                </div>
                <div class="row d-flex justify-content-center">
                    <div class="col-10 text-center">
                        <input type="submit" name="envoi" class="btn btn_form mb-3"></input>  
                    </div>           
                </div>
            </form>
        </div>
    
</body>
</html>