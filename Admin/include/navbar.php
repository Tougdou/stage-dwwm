<div id="top_screen" class="container-fluid p-0">    
    <nav class="navbar navbar-expand-lg navbar-dark p-0">
        <a class="mx-4 my-2 logo_main" href="../Index/index.php"><img src="../assets/icons/logo2.svg" alt=""></a>
        <button class="navbar-toggler me-3" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-center" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <ul class="navbar-nav text-white">
                    <li class="nav-item">
                    <a class="nav-link mx-4 my-2 text-white" href="../Index/index.php">Accueil</a>
                    <li class="nav-item">
                        <a class="nav-link mx-4 my-2 text-white" href="../Presentation/presentation.php">Présentation</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mx-4 my-2 text-white" href="../Informations/infos.php">Pour qui ?</a>
                    </li>
                    <a class="mx-4 my-2 logo_burger" href="../Index/index.php"><img src="../assets/icons/logo2.svg" alt=""></a>
                    <li class="nav-item">
                        <a class="nav-link mx-4 my-2 text-white" href="../Outils/outils.php">Mes outils</a>
                    </li>	
                    <li class="nav-item">
                        <a class="nav-link mx-4 my-2 text-white" href="../Articles/articles.php">Articles</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mx-4 my-2 text-white" href="../Contact/contact.php">Contact</a>
                    </li>
                </ul>
            </div>
        </div>   
    </nav>
</div>
