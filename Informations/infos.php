<?php include('../include/head.php'); ?>
<body class="bg-white">
<?php include('../include/navbar.php'); ?>

<header class="container-fluid">
    <div class="row">
        <div class="text-center text-white">
            <h1 class="text_header pt-5 mb-0" data-aos="fade-down" data-aos-duration="2000">Consulter un psychologue, c’est pour qui ?</h1>  
        </div>
    </div>
</header>
<div class="svg_top" style="background-color: #071a4c;">
    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
        <path fill="#1BAAB0" d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z" class="shape-fill"></path>
    </svg>
</div>

<div class="container-fluid section2 pb-5">
    <div class="container-fluid text-center pt-5 pb-5">
        <h2 class="titre_prat mb-5 pt-3">Un accompagnement psychologique pour chaque étape de la vie</h2> 
        <div class="d-flex justify-content-center text_prat">
            <p class="">À chaque âge de la vie, du nourrisson à la vieillesse, l’être humain traverse des moments plus ou moins délicats, où il se sent vulnérable et en difficulté.</p>
        </div>
        <div class="d-flex justify-content-center text_prat">
            <p class="">Il peut trouver une aide dans l’attention d’un psychothérapeute, l’écoute et l’accueil sans jugement de son vécu et de son mal-être.</p>
        </div>

    </div>
    <div class="row justify-content-center">
        <div class="card-container col-9 mt-3">
            <div class="text-center">
                <img src="assets/icons/fille.svg" style="width:200px;" alt="...">
            </div>   
            <h5 class="section2_title text-center fw-bold pb-3">Enfants</h5>
            <div class="section2_text" data-aos="fade-down" data-aos-duration="1000">
                <h3 class="fw-bold">Nourrisson :</h3>             
                <p class="pb-5">Dès leur plus jeune âge, mon approche systémique permet de comprendre leur comportement dans leurs premières semaines de vie. <br> 
                L’étude du système familial se fait donc en majorité avec les parents mais accompagné de leur nouveau né.</p> 
                
                <h3 class="fw-bold">Enfants entre 2 et 12 ans environ :</h3>
                <p class="pb-5">Le premier entretien s’effectue toujours avec les parents (ou au moins l’un des deux) afin d’éclaircir la demande et évoquer l’histoire de l’enfant. <br>
                Selon la problématique qui sera mise en lumière lors de ce premier entretien, je proposerai par la suite un entretien seul avec l’enfant ou en famille.</p> 
                
                <h3 class="fw-bold">Adolescents entre 13 et 18 ans :</h3> 
                <p class="pb-5">Même à cet âge, j’apprécie que le premier entretien soit en présence de l’un ou des deux parents. Cependant, certains adolescents préfèrent venir seul dès le début, ce qui est également envisageable. 
                Je précise qu’il y aura toujours, à un moment donné de sa thérapie, un lien à faire avec la famille pour que le travail avance au mieux.</p>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="card-container col-9 mt-3">
            <div class="">
                <div class="text-center"><img src="assets/icons/famille.svg" style="width:200px;" alt="..."></div>   
                <h5 class="section2_title text-center fw-bold pb-3">Adultes</h5>
                <div class="section2_text" data-aos="fade-down" data-aos-duration="1000">
                    <h3 class="fw-bold">Seul :</h3> 
                    <p class="pb-5">Pour travailler sur un moment de leur vie qui est douloureux à traverser, pour évoquer des questionnements sur leur fonctionnement relationnel, pour mieux se comprendre et donc mieux interagir par la suite avec l’autre, etc.</p>

                    <h3 class="fw-bold">En couple :</h3> 
                    <p>Pour aider l’un et l’autre à mieux se comprendre et ainsi mieux communiquer.</p>  
                    <p class="pb-5">Je précise que je ne suis pas thérapeute conjugal mais mes outils systémiques permettent d’apporter une certaine fluidité dans la relation.</p>

                    <h3 class="fw-bold">En famille :</h3> 
                    <p class="pb-5">Pour travailler la relation entre les membres de la famille et ainsi apaiser certaines tensions et / ou faire disparaître des symptômes qui se seraient cristallisés au fil des années. </p>
                </div>           
            </div>
        </div>
    </div>
</div>

<div class="custom-shape-divider-bottom-1633208821">
    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
        <path d="M321.39,56.44c58-10.79,114.16-30.13,172-41.86,82.39-16.72,168.19-17.73,250.45-.39C823.78,31,906.67,72,985.66,92.83c70.05,18.48,146.53,26.09,214.34,3V0H0V27.35A600.21,600.21,0,0,0,321.39,56.44Z" fill="#071a4c"></path>
    </svg>
</div>

<div class="container-fluid section3">
    <div class="row pt-5 pb-5 justify-content-center">
        <div class="col-lg-6 col-11 tarif-container mb-2">
            <div class="container-fluid">
                <div class="row">
                    <div class="d-flex p-0 justify-content-center mt-3">
                        <img class="mb-3 me-2" src="assets/icons/euros.svg" alt="">  
                        <p class="titre_tarif">Tarifs</p>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="d-flex">
                        <div class="col-md-10 col-9">
                            <p class="fw-bold">Enfant - Consultation de suivi de psychologie</p>
                        </div>
                        <div class="col-md-2 col-3 text-end">
                            <p class="fw-bold">65 €</p>
                        </div>                   
                    </div>
                </div>
                <hr class="mt-0">
                <div class="row">
                    <div class="d-flex">
                        <div class="col-md-10 col-9">
                            <p class="fw-bold">Adulte - Consultation de suivi de psychologie</p>
                        </div>
                        <div class="col-md-2 col-3 text-end">
                            <p class="fw-bold">65 €</p>
                        </div>                   
                    </div>
                </div>
                <hr class="mt-0">
                <div class="row">
                    <div class="d-flex">
                        <div class="col-md-10 col-9">
                            <p class="fw-bold">Consultation de thérapie de couple</p>
                        </div>
                        <div class="col-md-2 col-3 text-end">
                            <p class="fw-bold">65 €</p>
                        </div>                   
                    </div>
                </div>
                <hr class="mt-0">
                <div class="row">
                    <div class="d-flex">
                        <div class="col-md-10 col-9">
                            <p class="fw-bold">Consultation de thérapie familiale</p>
                        </div>
                        <div class="col-md-2 col-3 text-end">
                            <p class="fw-bold">65 €</p>
                        </div>                   
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-11 infotarif-container mb-2">
            <div class="container-fluid text-center fw-bold">
                <hr class="infotarif_trait">
                <p class="mt-5 fst-italic">Ces honoraires vous sont communiqués à titre indicatif par le praticien.</p>
                <p>Ils peuvent varier selon le type de soins finalement réalisés en cabinet, le nombre de consultations et les actes additionnels nécessaires. </p> 
                <p class="mb-5">En cas de dépassement des tarifs, le praticien doit en avertir préalablement le patient.</p>
                <hr class="infotarif_trait">
            </div>           
        </div>
    </div>
</div>

<?php include('../include/footer.php'); ?>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
  AOS.init();
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</body>
</html>