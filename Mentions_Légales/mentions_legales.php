<?php include('../include/head.php'); ?>
<body class="bg-white">
<?php include('../include/navbar.php'); ?>

<header class="container-fluid">
    <div class="row">
        <div class="text-center text-white">
            <h1 class="text_header pt-5 mb-0">Mentions Légales</h1>  
        </div>
    </div>
</header>
<div class="svg_top">
    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
        <path fill="#1BAAB0" d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z" class="shape-fill"></path>
    </svg>
</div>
<div class="container-fluid section1 mt-5 pt-5 ">
<h3 class="pb-5">Sur le site d'une société, on doit avoir les mentions suivantes :</h3>
<ul>
        <li>Dénomination sociale ou raison sociale</li>
        <li>Adresse du siège social</li>
        <li>Numéro de téléphone et adresse de courrier électronique</li>
        <li>Forme juridique de la société (SA, SARL, SNC, SAS, etc.)</li>
        <li>Montant du capital social</li>
        <li>Nom du directeur ou du codirecteur de la publication et celui du responsable de la rédaction s'il en existe</li>
        <li>Nom, dénomination ou raison sociale et adresse et numéro de téléphone de l'hébergeur de son site</li>
</ul>

<h3>Autres</h3>
    <ul>
        <li>Les mentions relatives à la propriété intellectuelle</li>
        <li>Les mentions relatives à l’hébergement du site</li>
    </ul>







</div>