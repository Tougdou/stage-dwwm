<footer>
<div class="custom-shape-divider-top-1636676615">
    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
        <path d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z" class="shape-fill"></path>
    </svg>
</div>

    <div class="container-fluid pt-3 text-white">
        <div class="row">
            <div class="col-12 col-lg-3 col-sm-6 order-lg-1 order-2 ps-5 d-flex flex-column align-items-end">
                <h2 class="footer_title">Horaires</h2>
                <hr id="footer_hr" class="footer_hr mb-4" style="margin-top: 8px;">
                    <p class="footer_horaires">Lundi : 12h - 17h</p>
                    <p class="footer_horaires">Mardi : 08h45 - 16h30</p>
                    <p class="footer_horaires">Mercredi : 10h - 19h</p>
                    <p class="footer_horaires">Jeudi : 09h15 - 19h</p>
                    <p class="footer_horaires">Vendredi : 11h - 19h30</p>
            </div>
            <div class="text-center col-12 col-lg-6  order-lg-2 order-1">
                <h2 class="footer_title text-center">Accès rapides</h2>
                <hr id="footer_hr" class="footer_hr_liens mb-4 text-center" style="margin-top: 1rem;"                                                  >
                    <a class="footer_liens ms-3 me-3" href="../Index/index.php">Accueil</a>
                    <a class="footer_liens ms-3 me-3" href="../Presentation/presentation.php">Présentation</a>
                    <a class="footer_liens ms-3 me-3" href="../Informations/infos.php">Pour qui ?</a>
                    <a class="footer_liens ms-3 me-3" href="../Outils/outils.php">Outils</a>
                    <a class="footer_liens ms-3 me-3" href="../Articles/articles.php">Articles</a>
                    <a class="footer_liens ms-3 me-3" href="../Contact/contact.php">Contact</a>
                    <div class="mt-5">
                        <a href="#top_screen">
                            <img src="../assets/icons/test.svg" alt="">
                        </a>
                        <h5 class="footer_text_top pt-3 pb-3">Haut de page</h5> 
                    </div>
            </div>
            <div class="col-12 col-lg-3  col-sm-6 order-lg-3 order-3 ps-5">
                <h2 class="footer_title">Contact</h2>
                <hr id="footer_hr" class="footer_hr mb-4">
                <div class="d-flex mb-3">
                    <a class="footer_liens_contact" href="tel:0648839413"><img src="../assets/icons/footer_tel.svg" alt=""></a>
                    <a class="footer_liens_contact" href="tel:0648839413"><p class="footer_contact ms-3">06 48 83 94 13</p></a>
                </div>
                <div class="d-flex">
                    <a class="footer_liens_contact" href="mailto:alexia.reynaud@live.fr"><img src="../assets/icons/footer_mail.svg" style="width:35px;height: 35px;" alt=""></a>
                    <a class="footer_liens_contact" href="mailto:alexia.reynaud@live.fr"><p class="footer_contact ms-3">alexia.reynaud@live.fr</p></a>
                </div>
                <div class="d-flex mb-1 mt-2">
                    <a class="footer_liens_contact" href="https://www.google.com/maps/place/25+Rue+du+G%C3%A9n%C3%A9ral+de+Gaulle,+17139+Dompierre-sur-Mer/@46.1883564,-1.0650474,17z/data=!4m13!1m7!3m6!1s0x48014d20ecc75c55:0xd6d1304b96786059!2s25+Rue+du+G%C3%A9n%C3%A9ral+de+Gaulle,+17139+Dompierre-sur-Mer!3b1!8m2!3d46.1882827!4d-1.064997!3m4!1s0x48014d20ecc75c55:0xd6d1304b96786059!8m2!3d46.1882827!4d-1.064997" target="blank"><img src="../assets/icons/footer_localisation.svg"></a> 
                    <a class="footer_liens_contact" href="https://www.google.com/maps/place/25+Rue+du+G%C3%A9n%C3%A9ral+de+Gaulle,+17139+Dompierre-sur-Mer/@46.1883564,-1.0650474,17z/data=!4m13!1m7!3m6!1s0x48014d20ecc75c55:0xd6d1304b96786059!2s25+Rue+du+G%C3%A9n%C3%A9ral+de+Gaulle,+17139+Dompierre-sur-Mer!3b1!8m2!3d46.1882827!4d-1.064997!3m4!1s0x48014d20ecc75c55:0xd6d1304b96786059!8m2!3d46.1882827!4d-1.064997" target="blank"> <p class="footer_contact ms-3">25, rue du Géneral de Gaulle <br> 17139 Dompierre sur Mer</a>
                </div>
                <div class="d-flex">
                    <img src="../assets/icons/footer_calendar.svg" alt="">
                    <p class="footer_contact ms-3">Ouvert du lundi au vendredi</p>
                </div>
            </div>
        </div>
        <hr id="hr_copyright" class="me-5 ms-5">
    </div>
    
    <div class="container-fluid text-center text-white">
        <div class="row">
            <div class="">
                <p class="copyright_text mb-0">Copyright &copy; 2021 - Tout droits réservés par Alexia Reynaud</p>
                <a class="copyright_links" href="../Mentions_Légales/mentions_legales.php"><p>Mentions Légales</p></a>
            </div>
        </div>
    </div>
    <a href="https://www.doctolib.fr/psychologue/dompierre-sur-mer/alexia-reynaud" style="text-align:center;background-color:#0596DE;color:#ffffff;font-size:14px;overflow:hidden;width:275px;height:40px;border-bottom-right-radius:none;border-bottom-left-radius:none;position:fixed;bottom:0;left:10px;z-index:1000;border-top-left-radius:4px;border-top-right-radius:4px;line-height:40px;font-size:1.1rem; text-decoration:none" target="_blank">Prendre rendez-vous<img style="height:25px;margin-bottom:3px" src="https://www.doctolib.fr/external_button/doctolib-white-transparent.png" alt="Doctolib"/></a>
</footer>
