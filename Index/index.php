<?php include('../include/head.php'); ?>
<body class="bg-white">
<?php include('../include/navbar.php'); ?>

<header class="container-fluid pt-5">
    <div class="row">
        <div class="text-center text-white mb-5">
            <p class="text_header mb-3" data-aos="fade-down" data-aos-duration="2000">Reynaud Belin Alexia</p>  
            <p class="text2_header mb-0" data-aos="fade-down" data-aos-duration="2000">Psychologue Clinicienne diplomée - Intervenante  systémicienne</p>
            <p class="text3_header pb-5" data-aos="fade-down" data-aos-duration="2000">Plus de 10 ans d'expériences en cabinet</p>    
        </div>
    </div>
</header>

<div class="container-fluid section1">
    <div class="row align-items-center">
        <div class="col-xl-5 col-lg-7 col-md-6 text-center" div data-aos="fade-right" data-aos-duration="1500"><img class="svg_section1" src="assets/icons/icons_sect1.svg" alt=""></div>
        <div class="col-xl-7 col-lg-5 col-md-6 text-center">
            <h1 class="pt-5">Lorem ipsum dolor sit amet.</h1>
            <h3>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur, deserunt?</h3>
            <h5 class="mt-5"><img src="../assets/icons/quote_left.svg" alt=""> Ne t’inquiète pas de l’échec. Inquiète-toi de ce que tu manques si tu n’essayes même pas.<img src="../assets/icons/quote_right.svg" alt=""></p>
            <a href="https://www.doctolib.fr/psychologue/dompierre-sur-mer/alexia-reynaud" target="_blank"><div class="btn btn_contact mb-3 ms-2 me-2 mt-5">Prendre rendez-vous</div></a>
        </div>
    </div>
</div>
<div class="custom-shape-divider-bottom-1633220128">
    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
        <path d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z" class="shape-fill"></path>
    </svg>
</div>
<div class="container-fluid section2">
    <div class="row justify-content-center">
        <div class="col-lg-3 col-10"> 
            <div class="card-body text-center">
                <img src="assets/icons/tel.svg" class="mt-3 mb-3" alt="...">
                <h5 class="titre_section2 text-center mb-3">Téléphone</h5>
                <a href="tel:+33648839413" class="text_num" style="text-decoration: none;">06 48 83 94 13</a>
            </div>
        </div>
        <div class=" col-lg-3 col-10">
            <div class="card-body text-center">
                <img src="assets/icons/horloge.svg" class="mt-3 mb-3" alt="..."> 
                <h5 class="titre_section2 text-center mb-3">Horaires</h5>  
                <h2 class="text_horaires">Ouvert du lundi au vendredi</h2>
            </div>
        </div>
        <div class=" col-lg-3 col-10">
            <div class="card-body text-center">
            <img src="assets/icons/mail.svg" class="mt-3 mb-3" alt="...">
                <h5 class="titre_section2 text-center mb-3">E-mail</h5>
                <a href="mailto:alexia.reynaud@live.fr" class="text_mail" style="text-decoration: none;">alexia.reynaud@live.fr</a>
            </div>
        </div>
    </div>
</div>
<div class="custom-shape-divider-top-1633220584">
    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
        <path d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z" class="shape-fill"></path>
    </svg>
</div>

<div class="container-fluid section3 pt-5 pb-5">
    <div class="container-fluid">
        <h3 class="text-center mb-5">Un accompagnement psychologique pour chaque étape de la vie </h3>
        <p class="text-center section_text">À chaque âge de la vie, du nourrisson à la vieillesse, l’être humain traverse des moments plus ou moins délicats, où il se sent vulnérable et en difficulté.</p>
        <p class="text-center section_text">Il peut trouver une aide dans l’attention d’un psychothérapeute, l’écoute et l’accueil sans jugement de son vécu et de son mal-être.</p>
    </div>
    <div class="row justify-content-center pt-5"> 
        <a href="../Informations/infos.php" target="_blank" class="card-container card col-xl-3 col-md-4 col-10 mt-5 ms-5 me-5" data-aos="flip-up" data-aos-easing="linear" data-aos-duration="500"> 
            <img src="assets/icons/enfant.svg" class="circle_card rounded-circle" style="width:150px; height: 150px;"  alt="...">         
            <div class="card-body fw-bold">          
                <h5 class="card-title text-center pt-5 mt-4 mb-4">Enfants</h5>
                <p class="card-text">* Nourrissons</p>
                <p class="card-text">* Les enfants entre 2 et 12 ans</p>
                <p class="card-text pb-3">* Les adolescents entre 13 et 18 ans</p>
            </div>
        </a>
        <a href="../Informations/infos.php" target="_blank" class="card-container card col-xl-3 col-md-4 col-10 mt-5 ms-5 me-5" data-aos="flip-up" data-aos-easing="linear" data-aos-duration="1000"> 
            <div class="">
                    <img src="assets/icons/adultes.svg" class="circle_card rounded-circle" style="width:150px; height: 150px;"  alt="...">
            </div>
                <div class="card-body fw-bold">      
                    <h5 class="card-title text-center pt-5 mt-4 mb-4">Adultes</h5>
                    <p class="card-text">* Seul</p>
                    <p class="card-text pb-3">* En couple</p>
                </div>
        </a>
        <a href="../Informations/infos.php" target="_blank" class="card-container card col-xl-3 col-md-8 col-10 mt-5 ms-5 me-5" data-aos="flip-up" data-aos-easing="linear" data-aos-duration="1500">
            <div class="">
                <img src="assets/icons/familles.svg" class="circle_card rounded-circle" style="width:150px; height: 150px;"  alt="...">
            </div>
            <div class="card-body fw-bold">    
                <h5 class="card-title text-center pt-5 mt-4 mb-4">Familles</h5>
                <p class="card-text">* Famille </p>
                <p class="card-text">* Famille recomposée</p>
                <p class="card-text pb-3">* Famille monoparental</p>
            </div>
        </a>
    </div>
</div>

<div class="container-fluid section4 mb-5">
    <div class="row justify-content-center align-items-center">
        <div class="col-md-4 col-4 text-center p-0">
            <!--Début Caroussel -->
            <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
                </div>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                    <img src="assets/images/puzzle.jpg" class="puzzle img-fluid" alt="...">
                    <div class="text-black fw-bold carousel-caption d-none d-md-block">
                        <h5>First slide label</h5>
                        <p>Some representative placeholder content for the first slide.</p>
                    </div>
                    </div>
                    <div class="carousel-item">
                    <img src="assets/images/puzzle.jpg" class="puzzle img-fluid" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>Second slide label</h5>
                        <p>Some representative placeholder content for the second slide.</p>
                    </div>
                    </div>
                    <div class="carousel-item">
                    <img src="assets/images/puzzle.jpg" class="puzzle img-fluid" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>Third slide label</h5>
                        <p>Some representative placeholder content for the third slide.</p>
                    </div>
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
            <!-- Fin Caroussel -->
        </div>
        <div class="mt-5 col-md-8 text-center">
            <h2 class="mb-3 fw-bold text-white">Listes de mes articles</h2>
            <h5 class="fw-bold fst-italic text-white">Vous retrouverez tout mes articles rédigés en appuyant sur le bouton juste en dessous.</h5>
            <a href="../Articles/articles.php" target="_blank"><div class="btn btn_contact2 mb-3 ms-2 me-2 mt-5 mb-5">Cliquez pour voir mes articles</div></a>
        </div>
    </div>
</div>

<div class="container-fluid section5 mt-5 mb-5">
    <div class="row justify-content-center align-items-center">
        <div class="mt-5 col-md-8 text-center">
            <h2 class="mb-3 fw-bold text-white">Avez-vous des questions ?</h2>
            <h5 class="fw-bold fst-italic text-white">Un formulaire de contact est disponible pour toutes interrogations autour de mon activité professionnelle.</h5>
            <a href="../Contact/contact.php" target="_blank"><div class="btn btn_contact3 mb-3 ms-2 me-2 mt-5 mb-5">Cliquez pour me contacter</div></a>
        </div>
        <div class="col-md-4 col-4 text-center p-0">
            <img class="puzzle img-fluid" src="assets/images/questions.jpg" alt="">
        </div>
    </div>
</div>


<?php include('../include/footer.php'); ?>

<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
  AOS.init();
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</body>
</html>