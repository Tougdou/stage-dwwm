<?php
session_start();
require_once '../Admin/config.php'; 

if (isset($_POST['submit'])) {
    if(!empty($_POST['nom']) AND !empty($_POST['email']) AND !empty($_POST['objet']) AND !empty($_POST['message'])){

        $nom = htmlspecialchars($_POST['nom']);
        $email = htmlspecialchars($_POST['email']);
       
        $pattern = "/^(\+33|0)[0-9]{9}$/"; //REGEX formatage tel
        $telephone = htmlspecialchars($_POST['telephone']); //Gestion caractères spéciaux
        $telephone = str_replace(' ','',$telephone); //Gestion des espaces

        $objet = htmlspecialchars($_POST['objet']);
        $message = htmlspecialchars($_POST['message']);

        if(empty($_POST['telephone']) || (preg_match($pattern, $telephone) == 1)) {
            
            $insertDataForm = $bdd->prepare('INSERT INTO contact(nom, email, telephone, objet, message, statut) VALUES (?,?,?,?,?,?)');
            $insertDataForm->execute(array($nom, $email, $telephone, $objet, $message, "nouveau")); 

            $_SESSION['success_form'] = "Formulaire envoyé avec succès";
        } else {
            $_SESSION['error_telephone'] = "Format téléphone incorrect";
        }
    }else{
        $_SESSION['error_form'] = "Veuillez remplir tous les champs du formulaire";
    }
}
?>

<?php include('../include/head.php') ?>
<body class="bg-white">
<?php include('../include/navbar.php') ?>

<header class="container-fluid">
    <div class="row">
        <div class="text-center text-white">
            <h1 class="text_header pt-5 mb-0" data-aos="fade-down" data-aos-duration="2000">Me Contacter</h1>  
        </div>
    </div>
</header>
<div class="svg_top">
    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
        <path fill="#1BAAB0" d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z" class="shape-fill"></path>
    </svg>
</div>
<div class="container-fluid section1 pt-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <p class="m-0 text_infos fw-bold">Vous souhaitez prendre rendez-vous pour une consultation en psychologie / psychothérapie pour vous ou votre enfant ? </p> 
                <p class="text_infos fw-bold pb-3"> Je vous invite à vous connectez sur la plateforme Doctissimo afin de trouver le créneau qui vous convient</p>
                <p class="text_infos">Pour toute demande complémentaires veuillez me joindre par mail ou téléphone</p>  
                <p class="fst-italic">Le formulaire de contact est également présent pour répondre à vos questions sur mes accompagnements.</p>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">  
    <div class="row">      
        <div class="offset-xl-1 col-xl-6">
            <h2 class="titre_form text-center">Formulaire de contact</h2>

<?php
    if (isset($_SESSION['error_form'])) {
        if ($_SESSION['error_form'] != NULL) {
            echo("<div class=\"alert alert-danger text-center mt-5\" role=\"alert\">" . $_SESSION['error_form'] . "</div>"); 
        } 
        unset($_SESSION['error_form']);
    }

    if (isset($_SESSION['error_telephone'])) {
        if ($_SESSION['error_telephone'] != NULL) {
            echo("<div class=\"alert alert-danger text-center mt-5\" role=\"alert\">" . $_SESSION['error_telephone'] . "</div>"); 
        } 
        unset($_SESSION['error_telephone']);
    }

    if (isset($_SESSION['success_form'])) {
        if ($_SESSION['success_form'] != NULL) {
            echo("<div class=\"alert alert-success text-center mt-5\" role=\"alert\">" . $_SESSION['success_form'] . "</div>"); 
        }
        unset($_SESSION['success_form']);
    }
?>

            <form  method="POST" data-aos="fade-right" data-aos-duration="2000">
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-10 col-10">
                        <label for="nom" class="form-label mt-3">Nom</label>
                        <input type="nom" class="form-control" id="nom" name="nom" placeholder="Votre nom">
                    </div>          
                </div>
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-5 col-5">
                        <label for="email" class="form-label mt-3">E-mail</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Adresse mail">
                    </div>
                    <div class="col-lg-5 col-5">
                        <label for="telephone" class="form-label mt-3">Téléphone</label>
                        <input type="telephone" class="form-control" id="telephone" name="telephone" placeholder="Numéro de téléphone (facultatif)">
                    </div>          
                </div>
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-10 col-10">
                        <label for="objet" class="form-label mt-3">Objet</label>
                        <input type="objet" class="form-control" id="objet" name="objet" placeholder="Objet du message">
                    </div>          
                </div>
                <div class="row d-flex justify-content-center mb-3">
                    <div class="col-lg-10 col-10">
                        <label for="message" class="form-label mt-3">Message</label>
                        <div>
                            <textarea class="col-12 champ_msg form-control" rows="6" id="message" name="message" placeholder="Votre message"></textarea>    
                        </div> 
                    </div>          
                </div>
                <div class="row d-flex justify-content-center">
                    <div class="col-10 text-center">
                        <input type="submit" name="submit" class="btn btn_form mb-3 me-1"></input>
                        <input type="reset" class="btn btn_form mb-3 ms-1" value="Réinitialiser"></input>    
                    </div>           
                </div>  
            </form>
        </div>  
        <div class="col-xl-5">
            <div class="row class justify-content-center">
                <div class="col-xl-10 col-md-6">
                    <h2 class="titre_horaires mb-3">Horaires d'ouverture</h2>
                    <p class="text_horaires">Lundi : 12h - 17h</p>
                    <p class="text_horaires">Mardi : 8h45 - 16h30</p>
                    <p class="text_horaires">Mercredi : 10h - 19h</p>
                    <p class="text_horaires">Jeudi : 09h15 - 19h</p>
                    <p class="text_horaires">Vendredi : 11h - 19h30</p>
                </div>
                <div class="col-xl-10 col-md-5 mt-5">
                    <div class="d-flex align-items-center">
                        <img class="mt-3" src="assets/icons/tel2.svg" style="width:40px;height: 40px;"  alt="">
                        <p class="num mb-0 ms-3">06 48 83 94 13</p> 
                    </div>
                    <div class="d-flex align-items-center">
                        <img class="mt-3" src="assets/icons/mail2.svg" style="width:40px;height: 40px;" alt="">
                        <p class="mail mb-0 ms-3">alexia.reynaud@live.fr</p>   
                    </div>
                    <div class="d-flex align-items-center">
                        <img class="mt-3" src="assets/icons/calendar.svg" style="width:40px;height: 40px;" alt="">
                        <p class="calendrier mb-0 mt-3 ms-3">Ouvert du lundi au vendredi</p>   
                    </div>
                </div> 
            </div>     
        </div>   
    </div>
</div>
<div class="svg_bottom mt-5">
    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
        <path fill="#071a4c" d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z" class="shape-fill"></path>
    </svg>
</div>
<div class="container-fluid section2 p-0">
    <div class="container">
        <p class="text-center map_txt pb-5">Adresse de mon cabinet :  <br> 25 Rue du Général de Gaulle, 17139 Dompierre-sur-Mer, France</p> 
        <p class="text-center info_txt pb-3">Accès handicapé / Parking gratuit à  proximité / Rez-de-chaussée</p>                
    </div>    
    <div class="container pb-3">
        <iframe class="map pb-5" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2762.114807621654!2d-1.065054!3d46.188274!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48014d20ecc75c55%3A0xd6d1304b96786059!2s25%20Rue%20du%20G%C3%A9n%C3%A9ral%20de%20Gaulle%2C%2017139%20Dompierre-sur-Mer%2C%20France!5e0!3m2!1sfr!2sus!4v1630334741521!5m2!1sfr!2sus" width="1920" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>   
    </div>
</div>

<?php include('../include/footer.php'); ?>

<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
AOS.init();
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</body>
</html>