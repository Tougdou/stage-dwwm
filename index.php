<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Alexia REYNAUD, Psychologue</title>
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link rel="stylesheet" href="Index/assets/style.css"> 
	<link rel="stylesheet" href="assets/style.css">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/images/onglet.png">
</head>
<body class="bg-white">
    <div id="top_screen" class="container-fluid p-0">    
        <nav class="navbar navbar-expand-lg navbar-dark p-0">
            <a class="mx-5 my-2 logo_main" href="Index/index.php"><img src="assets/icons/logo2.svg" alt=""></a>
            <button class="navbar-toggler me-3" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-center" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <ul class="navbar-nav text-white">
                    <li class="nav-item">
                    <a class="nav-link mx-4 my-2 text-white" href="Index/index.php">Accueil</a>
                    <li class="nav-item">
                        <a class="nav-link mx-4 my-2 text-white" href="Presentation/presentation.php">Qui suis-je ?</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mx-4 my-2 text-white" href="Informations/infos.php">Pour qui ?</a>
                    </li>
                    <a class="mx-4 my-2 logo_burger" href="Index/index.php"><img src="assets/icons/logo2.svg" alt=""></a>
                    <li class="nav-item">
                        <a class="nav-link mx-4 my-2 text-white" href="Informations/infos.php">Mes outils</a>
                    </li>	
                    <li class="nav-item">
                        <a class="nav-link mx-4 my-2 text-white" href="Articles/articles.php">Articles</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mx-4 my-2 text-white" href="Contact/contact.php">Contact</a>
                    </li>
                </ul>
            </div>
            </div>   
        </nav>
    </div>
    <header class="container-fluid pt-5">
    <div class="row">
        <div class="text-center text-white mb-5">
            <p class="text_header mb-3" data-aos="fade-down" data-aos-duration="2000">Reynaud Belin Alexia</p>  
            <p class="text2_header mb-0" data-aos="fade-down" data-aos-duration="2000">Psychologue Clinicienne diplomée - Intervenante  systémicienne</p>
            <p class="text3_header pb-5" data-aos="fade-down" data-aos-duration="2000">Plus de 10 ans d'expériences en cabinet</p>    
        </div>
    </div>
</header>

<div class="container-fluid section1">
    <div class="row align-items-center">
        <div class="col-xl-5 col-lg-7 col-md-6 text-center" div data-aos="fade-right" data-aos-duration="1500"><img class="svg_section1" src="assets/icons/icons_sect1.svg" alt=""></div>
        <div class="col-xl-7 col-lg-5 col-md-6 text-center">
            <h1>Lorem ipsum dolor sit amet.</h1>
            <h3>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur, deserunt?</h3>
            <p class="mt-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Cupiditate nam dolore ullam earum ab. Deleniti ducimus non nobis quaerat reiciendis blanditiis consectetur, magnam rem earum nisi nihil voluptate accusamus esse.</p>
            <a href="https://www.doctolib.fr/psychologue/dompierre-sur-mer/alexia-reynaud" target="_blank"><div class="btn btn_contact mb-3 ms-2 me-2 mt-5">Prendre rendez-vous</div></a>
        </div>
    </div>
</div>
<div class="custom-shape-divider-bottom-1633220128">
    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
        <path d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z" class="shape-fill"></path>
    </svg>
</div>
<div class="container-fluid section2">
    <div class="row justify-content-center">
        <div class="col-lg-3 col-10"> 
            <div class="card-body text-center">
                <img src="assets/icons/tel.svg" class="mt-3 mb-3" alt="...">
                <h5 class="titre_section2 text-center mb-3">Téléphone</h5>
                <a href="tel:+33648839413" class="text_num" style="text-decoration: none;">06 48 83 94 13</a>
            </div>
        </div>
        <div class=" col-lg-3 col-10">
            <div class="card-body text-center">
                <img src="assets/icons/horloge.svg" class="mt-3 mb-3" alt="..."> 
                <h5 class="titre_section2 text-center mb-3">Horaires</h5>  
                <h2 class="text_horaires">Ouvert du lundi au vendredi</h2>
            </div>
        </div>
        <div class=" col-lg-3 col-10">
            <div class="card-body text-center">
            <img src="assets/icons/mail.svg" class="mt-3 mb-3" alt="...">
                <h5 class="titre_section2 text-center mb-3">E-mail</h5>
                <a href="mailto:alexia.reynaud@live.fr" class="text_mail" style="text-decoration: none;">alexia.reynaud@live.fr</a>
            </div>
        </div>
    </div>
</div>
<div class="custom-shape-divider-top-1633220584">
    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
        <path d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z" class="shape-fill"></path>
    </svg>
</div>

<div class="container-fluid section3 pt-5 pb-5">
    <div class="container-fluid">
        <h3 class="text-center mb-5">Un accompagnement psychologique pour chaque étape de la vie </h3>
        <p class="text-center section_text">À chaque âge de la vie, du nourrisson à la vieillesse, l’être humain traverse des moments plus ou moins délicats, où il se sent vulnérable et en difficulté.</p>
        <p class="text-center section_text">Il peut trouver une aide dans l’attention d’un psychothérapeute, l’écoute et l’accueil sans jugement de son vécu et de son mal-être.</p>
    </div>
    <div class="row justify-content-center pt-5"> 
        <a href="Informations/infos.php" target="_blank" class="card-container col-xl-3 col-md-4 col-10 card mt-5 mb-5 ms-5 me-5" data-aos="flip-up" data-aos-easing="linear" data-aos-duration="500"> 
            <img src="assets/icons/enfant.svg" class="circle_card rounded-circle" style="width:150px; height: 150px;"  alt="...">         
            <div class="card-body fw-bold">          
                <h5 class="card-title text-center pt-5 mt-4 mb-4">Enfants</h5>
                <p class="card-text">* Nourrissons</p>
                <p class="card-text">* Les enfants entre 2 et 12 ans</p>
                <p class="card-text pb-3">* Les adolescents entre 13 et 18 ans</p>
            </div>
        </a>
        <a href="Informations/infos.php" target="_blank" class="card-container col-xl-3 col-md-4 col-10 card mt-5 mb-5 ms-5 me-5" data-aos="flip-up" data-aos-easing="linear" data-aos-duration="1000"> 
            <div class="">
                    <img src="assets/icons/adultes.svg" class="circle_card rounded-circle" style="width:150px; height: 150px;"  alt="...">
            </div>
                <div class="card-body fw-bold">      
                    <h5 class="card-title text-center pt-5 mt-4 mb-4">Adultes</h5>
                    <p class="card-text">* Seul</p>
                    <p class="card-text pb-3">* En couple</p>
                </div>
        </a>
        <a href="Informations/infos.php" target="_blank" class="card-container col-xl-3 col-md-8 col-10 card mt-5 ms-5 me-5" data-aos="flip-up" data-aos-easing="linear" data-aos-duration="1500">
            <div class="">
                <img src="assets/icons/familles.svg" class="circle_card rounded-circle" style="width:150px; height: 150px;"  alt="...">
            </div>
            <div class="card-body fw-bold">    
                <h5 class="card-title text-center pt-5 mt-4 mb-4">Familles</h5>
                <p class="card-text">* Famille </p>
                <p class="card-text">* Famille recomposée</p>
                <p class="card-text pb-3">* Famille divorcée</p>
            </div>
        </a>
    </div>
</div>

<div class="container-fluid section4 mb-5">
    <div class="row justify-content-center align-items-center">
        <div class="col-md-4 col-4 text-center p-0">
            <!--Début Caroussel -->
            <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
                </div>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                    <img src="assets/images/puzzle.jpg" class="puzzle img-fluid" alt="...">
                    <div class="text-black fw-bold carousel-caption d-none d-md-block">
                        <h5>First slide label</h5>
                        <p>Some representative placeholder content for the first slide.</p>
                    </div>
                    </div>
                    <div class="carousel-item">
                    <img src="assets/images/puzzle.jpg" class="puzzle img-fluid" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>Second slide label</h5>
                        <p>Some representative placeholder content for the second slide.</p>
                    </div>
                    </div>
                    <div class="carousel-item">
                    <img src="assets/images/puzzle.jpg" class="puzzle img-fluid" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>Third slide label</h5>
                        <p>Some representative placeholder content for the third slide.</p>
                    </div>
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
            <!-- Fin Caroussel -->
        </div>
        <div class="mt-5 col-md-8 text-center">
            <h2 class="mb-3 fw-bold text-white">Listes de mes articles</h2>
            <h5 class="fw-bold fst-italic text-white">Vous retrouverez tout mes articles rédigés en appuyant sur le bouton juste en dessous</h5>
            <a href="Contact/contact.php" target="_blank"><div class="btn btn_contact2 mb-3 ms-2 me-2 mt-5 mb-5">Cliquez pour voir mes articles</div></a>
        </div>
    </div>
</div>

<div class="container-fluid section5 mt-5 mb-5">
    <div class="row justify-content-center align-items-center">
        <div class="mt-5 col-md-8 text-center">
            <h2 class="mb-3 fw-bold text-white">Avez-vous des questions ?</h2>
            <h5 class="fw-bold fst-italic text-white">Un formulaire de contact est disponible pour toutes interrogations autour de mon activité professionnelle.</h5>
            <a href="Contact/contact.php" target="_blank"><div class="btn btn_contact3 mb-3 ms-2 me-2 mt-5 mb-5">Cliquez pour me contacter</div></a>
        </div>
        <div class="col-md-4 col-4 text-center p-0">
            <img class="puzzle img-fluid" src="assets/images/questions.jpg" alt="">
        </div>
    </div>
</div>
<footer>
<div class="custom-shape-divider-top-1636676615">
    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
        <path d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z" class="shape-fill"></path>
    </svg>
</div>

    <div class="container-fluid pt-3 text-white">
        <div class="row">
            <div class="col-12 col-lg-3 col-sm-6 order-lg-1 order-2 ps-5 d-flex flex-column align-items-end">
                <h2 class="footer_title">Horaires</h2>
                <hr id="footer_hr" class="footer_hr mb-4" style="margin-top: 8px;">
                    <p class="footer_horaires">Lundi : 12h - 17h</p>
                    <p class="footer_horaires">Mardi : 08h45 - 16h30</p>
                    <p class="footer_horaires">Mercredi : 10h - 19h</p>
                    <p class="footer_horaires">Jeudi : 09h15 - 19h</p>
                    <p class="footer_horaires">Vendredi : 11h - 19h30</p>
            </div>
            <div class="text-center col-12 col-lg-6  order-lg-2 order-1">
                <h2 class="footer_title text-center">Accès rapides</h2>
                <hr id="footer_hr" class="footer_hr_liens mb-4 text-center" style="margin-top: 1rem;"                                                  >
                    <a class="footer_liens ms-3 me-3" href="Index/index.php">Accueil</a>
                    <a class="footer_liens ms-3 me-3" href="Presentation/presentation.php">Présentation</a>
                    <a class="footer_liens ms-3 me-3" href="Informations/infos.php">Pour qui ?</a>
                    <a class="footer_liens ms-3 me-3" href="Outils/outils.php">Outils</a>
                    <a class="footer_liens ms-3 me-3" href="Articles/articles.php">Articles</a>
                    <a class="footer_liens ms-3 me-3" href="Contact/contact.php">Contact</a>
                    <div class="mt-5">
                        <a href="#top_screen">
                            <img src="assets/icons/test.svg" alt="">
                        </a>
                        <h5 class="footer_text_top pt-3 pb-3">Haut de page</h5> 
                    </div>
            </div>
            <div class="col-12 col-lg-3  col-sm-6 order-lg-3 order-3 ps-5">
                <h2 class="footer_title">Contact</h2>
                <hr id="footer_hr" class="footer_hr mb-4">
                <div class="d-flex mb-3">
                    <a class="footer_liens_contact" href="tel:0648839413"><img src="assets/icons/footer_tel.svg" alt=""></a>
                    <a class="footer_liens_contact" href="tel:0648839413"><p class="footer_contact ms-3">06 48 83 94 13</p></a>
                </div>
                <div class="d-flex">
                    <a class="footer_liens_contact" href="mailto:alexia.reynaud@live.fr"><img src="assets/icons/footer_mail.svg" style="width:35px;height: 35px;" alt=""></a>
                    <a class="footer_liens_contact" href="mailto:alexia.reynaud@live.fr"><p class="footer_contact ms-3">alexia.reynaud@live.fr</p></a>
                </div>
                <div class="d-flex mb-1 mt-2">
                    <a class="footer_liens_contact" href="https://www.google.com/maps/place/25+Rue+du+G%C3%A9n%C3%A9ral+de+Gaulle,+17139+Dompierre-sur-Mer/@46.1883564,-1.0650474,17z/data=!4m13!1m7!3m6!1s0x48014d20ecc75c55:0xd6d1304b96786059!2s25+Rue+du+G%C3%A9n%C3%A9ral+de+Gaulle,+17139+Dompierre-sur-Mer!3b1!8m2!3d46.1882827!4d-1.064997!3m4!1s0x48014d20ecc75c55:0xd6d1304b96786059!8m2!3d46.1882827!4d-1.064997" target="blank"><img src="assets/icons/footer_localisation.svg"></a> 
                    <a class="footer_liens_contact" href="https://www.google.com/maps/place/25+Rue+du+G%C3%A9n%C3%A9ral+de+Gaulle,+17139+Dompierre-sur-Mer/@46.1883564,-1.0650474,17z/data=!4m13!1m7!3m6!1s0x48014d20ecc75c55:0xd6d1304b96786059!2s25+Rue+du+G%C3%A9n%C3%A9ral+de+Gaulle,+17139+Dompierre-sur-Mer!3b1!8m2!3d46.1882827!4d-1.064997!3m4!1s0x48014d20ecc75c55:0xd6d1304b96786059!8m2!3d46.1882827!4d-1.064997" target="blank"> <p class="footer_contact ms-3">25, rue du Géneral de Gaulle <br> 17139 Dompierre sur Mer</a>
                </div>
                <div class="d-flex">
                    <img src="assets/icons/footer_calendar.svg" alt="">
                    <p class="footer_contact ms-3">Ouvert du lundi au vendredi</p>
                </div>
            </div>
        </div>
        <hr id="hr_copyright" class="me-5 ms-5">
    </div>
    
    <div class="container-fluid text-center text-white">
        <div class="row">
            <div class="">
                <p class="copyright_text mb-0">Copyright &copy; 2021 - Tout droits réservés par Alexia Reynaud</p>
                <a class="copyright_links" href="Mentions_Légales/mentions_legales.php"><p>Mentions Légales</p></a>
            </div>
        </div>
    </div>
    <a href="https://www.doctolib.fr/psychologue/dompierre-sur-mer/alexia-reynaud" style="text-align:center;background-color:#0596DE;color:#ffffff;font-size:14px;overflow:hidden;width:275px;height:40px;border-bottom-right-radius:none;border-bottom-left-radius:none;position:fixed;bottom:0;left:10px;z-index:1000;border-top-left-radius:4px;border-top-right-radius:4px;line-height:40px;font-size:1.1rem; text-decoration:none" target="_blank">Prendre rendez-vous<img style="height:25px;margin-bottom:3px" src="https://www.doctolib.fr/external_button/doctolib-white-transparent.png" alt="Doctolib"/></a>
</footer>

<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
  AOS.init();
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</body>
</html>