<?php include('../include/head.php'); ?>
<body class="">
<?php include('../include/navbar.php'); ?>

<header class="container-fluid pt-5">
    <div class="row">
        <div class="text-center text-white">
            <h1 class="text_header mb-0" data-aos="fade-down" data-aos-duration="2000">Qui suis-je ?</h1>  
        </div>
    </div>
</header>
<div class="svg_top" style="background-color: #071a4c;">
    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
        <path fill="#1BAAB0" d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z" class="shape-fill"></path>
    </svg>
</div>

<div class="container-fluid banniere_profil pt-5 px-0">
    <div class="row justify-content-center mx-0">
        <div class="col-md-3 col-8 mb-5 text-center">
            <img src="assets\images\profil2.jpg" class="profil rounded-circle img-fluid" alt="">
        </div>
        <div class="col-md-6 col-10 text-white">
            <h1 class="titre_profil fw-bold pb-3">Psychologue Clinicienne - Intervenante systémicienne</h1>
            <p class="text_profil">Je m'appelle Alexia Reynaud Belin, je suis mariée et j’ai deux enfants. <br>Depuis que je suis petite, je dis à mes parents : « j’aimerais faire un métier qui permet d’aider à comprendre comment les gens fonctionnent dans leur tête » … !</p> 
            <p class="text_profil">Quelques années plus tard, j’ai obtenu mon master de psychopathologie clinique en 2009 à l’Université Catholique de l’Ouest à Angers.</p>
            <p class="text_profil fw-bold">Me voilà aujourd’hui devenue psychologue clinicienne !</p>             
        </div>    
    </div>
</div>
<div class="container">
    <hr id="hr_profil">
</div>
<div class="container-fluid section2">
    <div class="container pt-5 pb-5">
        <p>Au début de ma carrière, j’ai travaillé au sein des crèches collectives et familiales de la municipalité de La Rochelle. <br> En quelques mots :</p>
        <ul>
            <li class="fw-bold">J’animais des groupes d’analyse de la pratique avec les professionnels de la petite enfance.</li>
            <li class="fw-bold">Je recevais tout parent qui avait besoin d’échanger sur le développement de son enfant. </li>
            <li class="fw-bold">J’accompagnais les équipes si un enfant leurs posait question. </li>
        </ul>
        <br>
        <p>En parallèle, je lançais mon activité en libéral sur un temps partiel. Plus je découvrais mon métier, et plus le système familial m’intéressait. <br> Je prends conscience au fil de mes entretiens que la relation entre les différents membres d’une famille est très riche sur le plan clinique !</p>
        <br>
        <p class="fw-bold">Mais d’ailleurs, qu'est ce que la clinique ? </p>
        <p>C'est tout ce dont le psychologue va se saisir pour trouver des pistes de travail avec vous (vos mots, vos gestes, votre façon d’interagir avec l’autre, ..). L’ensemble de votre être est riche de réponses à votre problématique mais vous n’en avez pas encore conscience. Il faut parfois un décodeur, c'est là que le psychologue intervient !</p> 
        <p>Je me suis donc spécialisée à la systèmie en 2017, c’est à dire à l’étude du système, et plus précisément à l’étude du fonctionnement familial.</p>
        <p class="fw-bold mb-0 pb-5">Pour résumer, aujourd’hui je reçois en individuel des enfants, adolescents et adultes mais je peux également recevoir simultanément tous les membres d’une même famille. </p>
    </div>    
</div>



<?php include('../include/footer.php'); ?>

<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
  AOS.init();
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</body>
</html>