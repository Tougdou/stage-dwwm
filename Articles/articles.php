<?php
session_start();
require_once '../Admin/config.php';
?>

<?php include('../include/head.php'); ?>
<body class="bg-white">
<?php include('../include/navbar.php'); ?>

<header class="container-fluid pt-5">
    <div class="row">
        <div class="text-center text-white">
            <h1 class="text_header mb-0" data-aos="fade-down" data-aos-duration="2000">Mes Articles</h1>  
        </div>
    </div>
</header>
<div class="svg_top" style="background-color: #071a4c;">
    <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
        <path fill="#1BAAB0" d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z" class="shape-fill"></path>
    </svg>
</div>

<?php
    $recupArticles = $bdd->query('SELECT * FROM articles ORDER BY articles.id DESC');
    while ($article = $recupArticles->fetch()){
    ?>  
        <div class="container-fluid section_articles ">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-8 col-10">
                    <div>
                        <h1 class="text-center py-5 px-5"><?= $article['titre']; ?></h1>
                        <p class="articles_contentu mx-5"><?= nl2br($article['contenu']); ?></p> 
                    </div>
                </div>
            </div>  
            <hr class="mb-0">
        </div>

    <?php
    }
?>

<?php include('../include/footer.php'); ?>

<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
  AOS.init();
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</body>
</html>